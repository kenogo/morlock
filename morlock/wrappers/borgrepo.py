# borgrepo.py
#
# Copyright 2019 Keno Goertz
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from datetime import datetime
import json
import logging
import os
import subprocess


class BorgRepo():
    """Borg repository class
    """
    STATUS_SUCCESS = 0
    STATUS_PATH_NOT_EMPTY = 2

    BORG_DATETIME_FORMAT = "%Y-%m-%dT%H:%M:%S.%f"

    # FIXME This is needed to stop borg from asking users for input, which is
    # incompatible with reading borg's output as json data (at least without
    # some major hacks). Hopefully, this will eventually be fixed in borg so
    # that this can be removed.
    AUTO_CONFIRMATION_ENV = {
        "BORG_UNKNOWN_UNENCRYPTED_REPO_ACCESS_IS_OK": "yes",
        "BORG_RELOCATED_REPO_ACCESS_IS_OK": "yes"
        }

    def __init__(self, path):
        logging.info("Repo path at %s" % path)
        self.path = path
        self.archives = None

    def init(self):
        logging.info("Initializing repository at %s" % self.path)
        result = subprocess.run(["borg", "init", "-e", "none", self.path])
        logging.debug("Initialization got return code %d" % result.returncode)
        return result.returncode

    def list(self):
        # Add automatic confirmation environment variables to the inherited
        # environment variables in order to stop borg from asking for user input
        env = os.environ.copy()
        env.update(self.AUTO_CONFIRMATION_ENV)
        logging.info("Listing repository contents of %s" % self.path)
        result = subprocess.run(
            ["borg", "list", "--json", self.path], capture_output=True, env=env)
        logging.debug("Listing got return code %d" % result.returncode)
        if result.returncode == self.STATUS_SUCCESS:
            parsed = json.loads(result.stdout)
            self.archives = [{"name": a["name"],
                              "time": datetime.strptime(
                                  a["time"], self.BORG_DATETIME_FORMAT)}
                             for a in parsed["archives"]]
        return result.returncode
