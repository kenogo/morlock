# main.py
#
# Copyright 2019 Keno Goertz
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import argparse
import logging
import gi

gi.require_version('Gtk', '3.0')

from gi.repository import Gtk, Gio

from .window import Window


class Application(Gtk.Application):
    def __init__(self):
        super().__init__(application_id='com.gitlab.kenogo.Morlock',
                         flags=Gio.ApplicationFlags.FLAGS_NONE)

    def do_activate(self):
        win = self.props.active_window
        if not win:
            win = Window(application=self)
        win.present()


def main(version):
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--log",
        help="Logging Level, one of [DEBUG, INFO, WARNING, ERROR, CRITICAL]",
        default="WARNING")
    args = parser.parse_args()
    log_level = getattr(logging, args.log.upper(), None)
    if not isinstance(log_level, int):
        raise ValueError('Invalid log level: %s' % args.log)
    logging.basicConfig(level=log_level)

    app = Application()
    return app.run()
